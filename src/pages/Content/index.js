let isLoaded = false;

function debounce(func, wait, immediate) {
    var timeout;
    return function () {
        var context = this, args = arguments;
        var later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};

const win = debounce(() => {
    if (!isLoaded) return;

    Array.from(document.getElementsByClassName('vote')).forEach(vote => {
        if (!vote.getAttribute('data-vote')) { vote.querySelector('[data-direction="up"]').click() }
    })
}, 100)

window.onload = () => {
    isLoaded = true
    MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
    const observer = new MutationObserver(function (mutations, observer) {
        win()
    });

    observer.observe(document, {
        subtree: true,
        attributes: true
    });

    win()

    window.onunload = () => { observer.disconnect() }

}

