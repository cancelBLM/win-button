import React from 'react';
import logo from '../../assets/img/logo.png';
import { Button } from '@material-ui/core';
import './Popup.css';

const Popup = () => {
  return (
    <div className="App">
      <header className="App-header">

        <Button
          onClick={() => {
            chrome.tabs.create({ 'url': "http://thedonald.win" }, function (tab) { })
          }}
          style={{ color: "#fbd420", margin: '2em' }}
        >

          <img src={logo} className="App-logo" alt="logo" />
        </Button>

      </header>
    </div >
  );
};

export default Popup;
